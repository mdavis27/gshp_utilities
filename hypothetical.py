from HeatPumps import PerformanceFunctions as PF
from HeatPumps import runtime

kW_aux=10.
cost_elec=0.15

LOAD=[10,15,20,25,30,35]
EWT=[20.,25.,40.,60.,80.,90.]

for ewt in EWT:
    for load in LOAD:
        Geo_OperCost=0.
        x=runtime.GEO(load,ewt)
        Geo_r_pl,Geo_r_fl,Geo_r_aux=x
        if all(isinstance(a,float) for a in x):
            Geo_OperCost=cost_elec*(
                PF.Geo_kW_pl(ewt)*Geo_r_pl +
                PF.Geo_kW_fl(ewt)*Geo_r_fl +
                kW_aux * Geo_r_aux)
            Geo_CostPerMMBTU=(Geo_OperCost/(load/1000.))
        else:
            Geo_OperCost='System Failed'
            Geo_CostPerMMBTU='System Failed'
        print load,ewt,Geo_r_pl,Geo_r_fl,Geo_r_aux,Geo_OperCost,Geo_CostPerMMBTU

OAT =[-10,-5,5.,10.,20.,30.,40.,50.,]
for oat in OAT:
    for load in LOAD:
        ASHP_OperCost=0.
        x=runtime.ASHP(load,oat)
        ASHP_r,ASHP_r_aux=x
        if all(isinstance(a,float) for a in x):
            ASHP_OperCost=cost_elec*(
                PF.ASHP_kW(oat)*ASHP_r +
                kW_aux * ASHP_r_aux)
            ASHP_CostPerMMBTU=(ASHP_OperCost/(load/1000.))
        else:
            ASHP_OperCost='System Failed'
            ASHP_CostPerMMBTU='System Failed'
        print load,oat,ASHP_r,ASHP_r_aux,ASHP_OperCost,ASHP_CostPerMMBTU

