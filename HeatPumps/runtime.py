import PerformanceFunctions as PF

HC_aux=3.412*10
'''
Determine heat pump runtime based on bin LOAD and heat pump capacity
    First, check to see if LOAD can be handled by PartLoad (pl)
    If not, apportion as much LOAD to FullLoad operation as possible and the
    remainder to aux.

    LOAD can be over 1 hour or 8 hour periods.  Here it taken as 1 hour (1.)

'''

def GEO(LOAD,ewt):
    Geo_r_pl=0.
    Geo_r_fl=0.
    Geo_r_aux=0.
    if ewt>22.5:
        if LOAD < 1.*PF.Geo_HC_pl(ewt):
            Geo_r_pl=LOAD/PF.Geo_HC_pl(ewt)
        elif LOAD < 1.*PF.Geo_HC_fl(ewt):
            Geo_r_fl=LOAD/PF.Geo_HC_fl(ewt)
        elif LOAD < 1.*(PF.Geo_HC_fl(ewt)+HC_aux):
            Geo_r_fl=1.
            Geo_r_aux=(LOAD-1.*PF.Geo_HC_fl(ewt))/HC_aux
        else:
            Geo_r_aux='Auxiliary insufficient'

    elif ewt<=22.5:
        if LOAD < 1.*HC_aux:
            Geo_r_aux=LOAD/HC_aux
        else:
            Geo_r_aux='Auxiliary insufficient'
    return Geo_r_pl,Geo_r_fl,Geo_r_aux

def ASHP(LOAD,oat):
    ASHP_r=0.
    ASHP_r_aux=0.
    if LOAD < 1.*PF.ASHP_HC(oat):
        ASHP_r = LOAD/PF.ASHP_HC(oat)
    elif LOAD < 1.*(PF.ASHP_HC(oat)+HC_aux):
        ASHP_r=1.
        ASHP_r_aux=(LOAD-1.*PF.ASHP_HC(oat))/HC_aux
    else:
        ASHP_r_aux='Auxiliary insufficient'

    return ASHP_r,ASHP_r_aux
