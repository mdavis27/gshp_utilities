'''
Initially, these parameters are hardwired.  We will want to develop a database at some point.
'''


a=0.1041
b=-0.008862
c=-0.0001153
d=0.02817

ASHP_HC_rated=36.
HSPF_rated=9.0

'''
These functions computes the geothermal heat pump performance data
for a given LOAD (total BTUs over 8 hour period), and entering water temperature.

Usage:
   runtime(load,ewt) returns the hours spent in part load, full load, and aux

   These runtimes are then used with the kW performance data to compute kWh
   and operating cost.

   A financial penalty for system failure can be added
   (e.g. $200 for each 8 hour period)
'''


def Geo_kW_pl(ewt):
    Geo_kW_pl=0.0019*ewt + 1.508
    return Geo_kW_pl

def Geo_kW_fl(ewt):
    Geo_kW_fl=0.0126*ewt + 1.829
    return Geo_kW_fl

def Geo_HE_fl(ewt):
    Geo_HE_fl=0.4233*ewt + 9.9826
    return Geo_HE_fl

def Geo_HE_pl(ewt):
    Geo_HE_pl=0.3241*ewt + 5.6288
    return Geo_HE_pl

def Geo_COP_pl(ewt):
    Geo_COP_pl=0.0244*ewt + 1.48
    return Geo_COP_pl

def Geo_COP_fl(ewt):
    Geo_COP_fl=0.0317*ewt + 2.96
    return Geo_COP_fl

def ASHP_COP(oat):
    Diff = a + b*oat + c*(oat)**2 + d*HSPF_rated
    HSPF_adj = (1-Diff)*HSPF_rated
    ASHP_COP=HSPF_adj/3.412
    return ASHP_COP

def ASHP_HC(oat):
    if oat>5.:
        ASHP_HC=ASHP_HC_rated
    else:
        ASHP_HC=ASHP_HC_rated*(0.0139*oat + 0.9289)
    return ASHP_HC

def ASHP_kW(oat):
    ASHP_kW=(ASHP_HC(oat))/(3.412*ASHP_COP(oat))
    return ASHP_kW

def Geo_HC_pl(ewt):
    Geo_HC_pl=3.41*Geo_kW_pl(ewt)+Geo_HE_pl(ewt)
    return Geo_HC_pl

def Geo_HC_fl(ewt):
    Geo_HC_fl=3.41*Geo_kW_fl(ewt)+Geo_HE_fl(ewt)
    return Geo_HC_fl
